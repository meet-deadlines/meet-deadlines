ARG BASE_IMAGE_VERSION=0.115.3
ARG DOCKER_REGISTRY=registry.gitlab.com

FROM ${DOCKER_REGISTRY}/belyaev.stanislav/live/hugo-builder:${BASE_IMAGE_VERSION} as builder

ADD . /app
WORKDIR /app

RUN hugo

FROM alpine:3.15.4

RUN apk update && \
    apk add nginx --no-cache 

COPY --from=builder /app/public /app
COPY root /

STOPSIGNAL SIGTERM
CMD ["/usr/sbin/nginx"]



