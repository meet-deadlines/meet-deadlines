---
title: "Завтрак проектных менеджеров / Алмата"
date: 2024-11-02T05:00:00+05:00
draft: false

toc:
  enable: false


speaker:
  name: ""
  email:
  telegram: 
  photoUrl: 
  facebook: 


---
Завтрак проектных менеджеров сообщества [@Projects_KZ](https://t.me/Projects_KZ)

- Дата: **02 ноября 2024 в 10.00** (Прошел)

![](/images/pm-breakfast-202411/1.jpg)

![](/images/pm-breakfast-202411/2.jpg)
