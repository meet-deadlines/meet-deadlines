---
title: "Ведение проектов в бирюзовой организации"
date: 2021-02-24T10:00:00+05:00
lastmod: 2021-07-11T22:00:00+05:00
draft: false
toc:
  enable: false
type: "events"
video: https://youtu.be/4OzswL420ps
presentation: 
author: "Станислав Беляев"
authorLink: "https://belyaev.live"
linkToMarkdown: false
images:
  - https://storage.yandexcloud.net/meet-deadlines/events/20210224/main.page.jpg

speaker:
  name: "Станислав Беляев"
  email:
  telegram:
  photoUrl: https://storage.yandexcloud.net/meet-deadlines/speakers/stanislav.belyaev/m0ZZkCD6_400x400.jpeg
  facebook: 

---

Станислав Беляев - занимается развитием ИТ-специалистов в [Точке](https://tochka.com)

Если кратко, то цветовая индикация показывает уровень развития организации (или менеджмента, уровень процессов). 

Кратко:
- Красная - это когда работает только авторитарный стиль управления и основной ресурс - это сила (власть)
- Оранжевая (еще дополним)
- Коричневая (еще дополним)
- Янтарная - это организации, где управление выстроено по KPI, есть делегирование ответственности на уровень среднего менеджмента по принятию решений
- Зеленая - это организация, где есть руководство, но у нее преобладают ценности в виде доброго отношения друг к другу, горизонтальная структура, важно действие, а не власть. Чем больше отдельное лицо вкладывается в работу организации, чем сильнее разделяет ценности - тем у него больше уважения
- Бирюзовая - организация с отсутствующей структурой компании, отсутствует власть, отсутствуют руководители, отсутствуют должности (они не имеют ценности). Часто называют самоорганизующейся компанией. Важнее ценности компании, отношение к миссии компании, все решения принимаются командой, а не руководством (в том числе зарплата)

Примеры расписаны [тут](https://intuition.team/arutyunov/?go=all/raznocvetnye-organizacii/)

Материалы, которые упомянуты выложить:
1. Книга Фредерика Лалу "[Открывая организации будущего](https://www.litres.ru/frederik-lalu/otkryvaya-organizacii-buduschego/)" - рассказывает про цветовую индикацию и как работают все процессы в бирюзовой организации 

2. [Интервью Бориса Дьяконова](https://www.youtube.com/watch?v=101BRR7FiVM) о том, почему он решил сделать Бирюзовую организацию и о его личных принципах, которые трансформируются в бизнесе, в отношении к людям

3. [Большое Демо](https://www.youtube.com/watch?v=JuXtTkFbquU&t=1s), где мы рассказывали о наших достижения и факапах за год. По своей сути - можно увидеть, а что можно сделать в такой структуре, без классического проектного управления

4. Еще, у нас есть [Конституция](https://github.com/tochka-public/Holacracy-Constitution-RUSSIAN/blob/master/Holacracy-Constitution-RU.md). Конституция нам нужна, чтобы система существовала. Это краткий свод правил, которыми мы руководствуемся. Мы обращаемся к конституции, когда у нас возникают спорные моменты или неразрешимые ситуации. На любом уровне. В том числе, здесь есть ответ на вопрос - а как работать с саботажем :)


{{< youtube 4OzswL420ps >}}