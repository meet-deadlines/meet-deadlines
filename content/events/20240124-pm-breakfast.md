---
title: "Завтрак проектных менеджеров / Алмата"
date: 2024-01-27T19:00:00+05:00
draft: false

toc:
  enable: false


speaker:
  name: ""
  email:
  telegram: 
  photoUrl: 
  facebook: 


---
Завтрак проектных менеджеров сообщества [@Projects_KZ](https://t.me/Projects_KZ)

![](/images/breakfast-202401/1.jpeg)

- Где: [**Coffeedelia, ​ул. Кабанбай батыра, 79**](https://go.2gis.com/4viem)
- Дата: **27 января 2024 в 10.00** (Прошел)
- Вход свободный, без регистрации

![](/images/breakfast-202401/1.jpeg)
