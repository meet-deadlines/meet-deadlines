---
title: "Мотивация, Культура - свободное обсуждение"
date: 2021-09-17T19:41:31+03:00
lastmod: 2022-04-10T19:41:31+03:00
draft: false

toc:
  enable: false

---

Мы говорили сегодня о многом. Сложно выделить отдельную тему:
- Отдельно про мотивацию
- немного про культуру
- совета как выстроить иерархию в организации
- как быть начинающему стартаперу в компании 🙂

Все это мы постарались уложить в 1 час.

Спасибо [@Osnovateel](https://t.me/NikolaMorozzov) за старт этой темы и обсуждения и тем, кто делился своим опытом сегодня:
[@stepan_chernov](https://t.me/stepan_chernov) [@NurbolBekmurza](https://t.me/NurbolBekmurza) [@maxinum](https://t.me/maxinum) 


{{< telegram "Projects_KZ/5737" >}}