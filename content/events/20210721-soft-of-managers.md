---
title: "Виды менеджеров и где они обитают"
date: 2021-07-18T10:00:00+05:00
lastmod: 2021-07-21T10:30:00+05:00
draft: false
toc:
  enable: false

type: "events"
video: https://youtu.be/d3Z9losz7Rw
presentation: 
author: "Станислав Беляев"
authorLink: "https://belyaev.live"

speaker:
  name: "Станислав Беляев"
  email:
  telegram:
  photoUrl: https://storage.yandexcloud.net/meet-deadlines/speakers/stanislav.belyaev/m0ZZkCD6_400x400.jpeg
  facebook: 

---

Мы успели посмотреть только 3 роли:
- Тимлид команды разработки
- Engineering Manager (Инженерный менеджер)
- Проектный менеджер 3х уровней компетенций:
  - Небольших проектов (чаще веб-студии)
  - Классический Менеджер проекта (который к PMBoK близок)
  - Администратор проекта

У нас не было презентации - у нас был [mindmap](https://mm.tt/1965453702?t=CC3EHz7iNM)

Дополнительный материал, который я демонстрировал и упоминал:
- Краткий ролик - [кто такой тимлид](https://www.youtube.com/watch?v=dNb7NIL42BE) - [Карта развития Тимлида](https://tlroadmap.io/) - собрано огромное количество компетенций тимлида - очень полезно посмотреть, рекомендую глянуть, даже если вы не Тимлид :)
- Чуть глубже разбор - [Как подсидеть Тимлида](https://www.youtube.com/watch?v=xL9zM09NDTk&t=4s) - рассматривают разные роли тимлидов в компаниях, их зарплаты и обязательства
- [Кто такой Инженерный менеджер](https://www.youtube.com/watch?v=Fb8pwgtRpFs), какие функции выполняет - доклад на 40 минут. Если вы не знаете кто это такой - рекомендую ознакомиться. Эта роль новая для Казахстана и России - существую в западных странах
- Описание роли [Администратор проекта](http://projectimo.ru/komanda-i-motivaciya/administrator-proekta.html) - не всегда она нужна. Возможно ваша компания не доросла до нее :)

{{< youtube d3Z9losz7Rw >}}
