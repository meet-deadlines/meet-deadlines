---
title: "Управление проектами по анализу данных на экспорт"
date: 2021-07-09T10:00:00+05:00
draft: false
toc:
  enable: false
ReadingTime: 1000
type: "events"
video: https://youtu.be/XiD8bDtlap4
presentation: https://storage.yandexcloud.net/meet-deadlines/events/20210709/20210709-azat-yesenov.pdf
author: "Азат Есенов"
authorLink: "https://www.facebook.com/azat.yessenov"

speaker:
  name: "Азат Есенов"
  email:
  telegram:
  photoUrl: https://storage.yandexcloud.net/meet-deadlines/speakers/azat.yesenov/azat.yesenov.jpeg
  facebook: azat.yessenov

---

**Почему мы рекомендуем послушать эту запись?**

Есенов Азат - управляющий партнер Maxinum Consulting Group и вместе с командой  занимается анализом и визуализацией данных для зарубежных клиентов. С 2017 года компания завершила 100+ проектов по Google Data Studio и Power BI

Азат рассказал об опыте работы с локальными заказчиками и зарубежными. Подсветил проблемы, с которыми сталкивается как компания. Сложности, которые нужно решать не только на уровне компании, но и на уровне государства! 
Основные - это Знание языка, Soft-skills инженеров и рынок. Но Азат не сдается и верит в то, что получит развитие :)

Азат в Презентации упомянул статью [Как работать на Upwork](https://www.facebook.com/azat.yessenov/posts/10220239900483654).

Презентацию можно скачать по [ссылке](https://storage.yandexcloud.net/meet-deadlines/events/20210709/20210709-azat-yesenov.pdf).


{{< youtube XiD8bDtlap4 >}}