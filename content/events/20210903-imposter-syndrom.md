---
title: "Синдром самозванца"
date: 2021-09-03T19:41:31+03:00
lastmod: 2022-04-10T19:41:31+03:00
draft: false

toc:
  enable: false

---

Напомню, что сегодня будем обсуждать вот такие случаи:
- Я не достоин этой зарплаты
- То, что я стал руководитель - это счастливая случайность, а не мои успехи
- Я не эксперт, чтобы говорить об этом
- Смысл об этом рассказывать, это и так известно всем?

Часто, такие мысли бродят в голове индивида с синдромом самозванца

Разберем сегодня теорию, случаи, что с этим делать. Отлично, если вы придете с вашими жизненными случаями и поделитесь опытом. 

p.s. но не забываем, что мы не психологи =)

{{< telegram "Projects_KZ/5363" >}}