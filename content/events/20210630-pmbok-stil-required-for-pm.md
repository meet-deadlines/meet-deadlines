---
title: "Почему вам все ещё понадобиться PMBOK"
date: 2021-06-30T00:00:00+05:00
lastmod: 2021-07-10T20:20:00+05:00
draft: false
toc:
  enable: false
type: "events"
video: https://youtu.be/lKP3fNHSOKQ
presentation: https://storage.yandexcloud.net/meet-deadlines/events/20210701/space-eq.pdf
author: "Степан Чернов"
authorLink: "https://linkedin.com/in/stepanchernov"

speaker:
  email:
  telegram:
  photoUrl: https://storage.yandexcloud.net/meet-deadlines/speakers/stepan.chernov/stepan.chernov.jpg 
  

---

**Почему мы рекомендуем послушать эту запись?**

Степан Чернов - Директор Стратегии, АО ДБ "Альфа-Банк"

Степан рассказал о трех кейсах для разного уровня менеджеров (молодые и опытные) и разных размеров проектов. Каждая история имеет завязку, сюжет и проблемы, с которыми проекты сталкивались. Степан делился что можно было бы улучшить, если бы люди знали как идут процессы по PMBOK :)

Напомнил, что же такое PMBOK и как его нужно воспринимать.

Сессия вопросов и ответов заслуживает отдельного внимания, потому что Степан напомнил, зачем в принципе живет бизнес (ответ банальный, но про него забывают) и что ответственность в реализации проекта лежит не только на Менеджере проекта (хоть ему и делегировали ответственность). Обсуждения очень хорошие )

Презентацию можно скачать по [ссылке](https://storage.yandexcloud.net/meet-deadlines/events/20210709/20210709-azat-yesenov.pdf).


{{< youtube lKP3fNHSOKQ >}}