---
title: "Опыт применения ИТ-подходов в кибербезопасности"
date: 2021-05-27T10:00:00+05:00
lastmod: 2021-07-11T22:00:00+05:00
draft: false
toc:
  enable: false
type: "events"
video: 
presentation: 
author: "Всеволод Шабад"
authorLink: "https://t.me/vshabad_kz"
linkToMarkdown: false
images:
  - https://storage.yandexcloud.net/meet-deadlines/speakers/vsevolod.shabad/photo_2021-05-27_13-55-50.jpg

speaker:
  name: "Всеволод Шабад"
  email:
  telegram:
  photoUrl: https://storage.yandexcloud.net/meet-deadlines/speakers/vsevolod.shabad/photo_2021-05-27_13-55-50.jpg
  facebook: 

---


Спикер: **Шабад Всеволод** - Советник Председателя Правления АО "Народный Банк Казахстана" (Halyk Bank)

Записи нет :( Будем ждать повтора встречи