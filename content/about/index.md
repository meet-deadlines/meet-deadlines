---
title: "О сообществе"
date: 2021-06-02T11:04:49+08:00
draft: false

lightgallery: true

math:
  enable: true
---

Сообщество **Projects KZ** - небольшое сообщество людей, увлеченных проектным менеджментом. Мы очень локальные - расположены в [Казахстане](https://ru.wikipedia.org/wiki/%D0%9A%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD). 

Наша миссия - Развивать людей, через построения сообщества. Сообщества и технологии в Казахстане приходят с запаздыванием к нам. За счет этого мы обладает большим потенциалом для развития. Догонять значительно проще, чем создавать новое и одна из наших целей - догнать по знаниям и компетенциям более развитые сообщества.

Нам предстоит еще переродиться, пересмотреть наши взгляды и оценить результаты действительно. Для нас главное - это постоянное движение, постоянное развитие участников, кто присоединился к нам. Мы верим, что каждый участник сообщества обладает своим уникальным опытом, который интересен другим. Цель сообщества - поспособствовать обмену этого опыта

## Что можно сделать?

Мы ожидаем активного участника в дискуссиях, обмене опыта [в нашем чате](https://t.me/Projects_KZ). Не стесняйтесь рассказать о себе или рассказать о вашем проекте. Периодически вознкиают дискуссии, участвуйте в них! С нашей стороны только одна просьба - соблюдать [правила](/rules) и проявлять уважение к участникам!

Мы с удовольствием поможем организовать митап, где вы сможете поделиться знаниями с аудиторией, а мы сохраним это в памяти на нашем [канале](https://www.youtube.com/channel/UCRiR9gsxY1AjZqKs8f7N1iA) и на нашем [ресурсе](/events).

Вам нужно помочь с оценкой вашей работы или резюме? Кажется, что сообщество - это тот самый случай, когда вы можете получить независимое и разностороннее мнение :). 

Если вы хотите стать активным участником, поделиться статьей или внести исправления в наш ресурс - мы тоже это приветсвует! Наш ресурс - это общественное владение и внести исправления может любой. Посмотрите как это [организовано]({{< relref "posts/how-to-add-content.md" >}}).

Сообщество становится тогда, когда участники не бояться делиться своим опытом! Не стесняйтесь, [расскажите](https://t.me/Projects_KZ) об успехах и неудачах прошедшей недели :)

## История

Сообщество родилось как [чат Projects KZ в Телеграмме](https://t.me/Projects_KZ) и это остается основной нашей площадкой. Мы молодые - появились в ноябре 2020 года, благодаря организации конференции по проектному управлению (кажется, что сообщества по интересам именно так и появляются). 

После конференции, осталось сообщество, но не было интереса у сообщества к тому, чтобы делиться знаниями. Попытки исследовать аудиторию натолкнули на мысль, что в сообществе много тех, кому интересно проектное управление, но нет достаточных знаний и опыта, чтобы активно участвовать в диалоге. 

Активными участниками сообщества мы решили исправить эту ситуацию и организовали периодические митапы темы близкие к проектному управлению. Здесь мы публикуем результаты наших встреч :)

## Ресурс meet-deadlines.kz

Июль 2021. Мы выросли из чата :) Информация стала теряться в истории, участники быстро теряют рекомендации и записи мероприятий и мы создали ресурс - meet-deadlines.kz. Ресурс поддерживается сообществом - каждый участник [может внести](/how-to-add-content) в него изменения. Не стесняйтесь!


## Наши ресурсы

- Чат в Телеграмм - [Projects_KZ](https://t.me/projects_kz) - здесь основные дискуссия и обсуждения
- Ресурс [meet-deadlines.kz](/) - здесь мы публикуем результаты общения
- [Канал YouTube Meet Deadlines](https://www.youtube.com/channel/UCRiR9gsxY1AjZqKs8f7N1iA) - подписывайтесь :) здесь мы публикуем записи наших мероприятий
- Наш [репозиторий](https://gitlab.com/meet-deadlines) - здесь хранятся ресурсы сообщества, в том числе исходники данного ресурса. Как наполнять - смотрите [раздел](/how-to-add-content)
## Аудитория

Мы проводим периодические исследования нашей аудитории:
- [Февраль 2021](/who-we-are-202103)