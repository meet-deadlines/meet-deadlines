---
title: "Сообщество Management Asia / Entry level"
date: 2024-10-28T14:39:46+03:00
draft: false

toc:
  enable: false

description: "Сообщество для тех, кто только входит в профессию"
---

На прошлой неделе [обсуждали](https://t.me/Projects_KZ/75470) проблему, что низкая вовлеченность участников в сообществе. Одна из гипотез была - новичкам, кто недавно в профессии - сложно задать вопрос, потому что вопрос может показаться глупым. Поэтому лучше промолчать

Это абсолютно противоположно тому, что должно делать сообщество, поэтому попробуем исправить эту проблему. Новый чат:

**Management Asia / Entry level**: [@ProjectsKZ_Entry](https://t.me/ProjectsKZ_Entry)

Будем сдерживать там флудинг, оставим его для вопрос от любого участника на темы менеджмента, проектного управления, управления командами. 

Если вы находите себя оторванным от контекста обсуждения этого чата - возможно, там будет проще находиться в контексте. 

Нет глупых вопросов, есть незаданные вопросы!