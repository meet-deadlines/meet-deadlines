---
title: "Полезные дискуссии чата Предпринимай"
date: 2022-05-14T14:39:46+03:00
lastmod: 2022-05-14
draft: true
hiddenFromHomePage: true

toc:
  enable: false
---

На этой страницы собраны ссылки на треды дискуссий в [чате](https://t.me/startup_connect), переходя по ним вы сможете найти интересные вам треды
<!--more--> 

{{< admonition type=info title="Откуда контент" open=false >}}
Контент страницы попролняется с помощью бота https://t.me/MessagesSaveBot в чате
{{< /admonition >}}

- [Как мы сообществом создали полезного бота](https://t.me/c/1259699598/21137)
