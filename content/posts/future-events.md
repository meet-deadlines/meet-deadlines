---
title: "ИТ и Digital мероприятия в Казахстане"
date: 2023-12-11T11:54:19+05:00
draft: false

toc:
  enable: false

---

Данная страница содержит список будущих ИТ-мероприятий, проводимых в Казахстане (если оффлайн) и в странах СНГ (если онлайн).

Данной страницей мы пытаемся ответь на вопрос - а куда можно сходить Продакту / Проджекту / Маркетологу для создания нетворкинга, какие события проводятся. 

- [Google Docs](https://docs.google.com/spreadsheets/d/1NFRR9Kuf_ByfoxZcbGItrqXmYgkGJJSwh2YHSbSx938/edit#gid=1432021575)
- [Канал в Telegram](https://t.me/digital_events_mig)

Собирает Александр Денисов {{<profile  telegram="grinvind">}}. Если вы хотите добавить ваше мероприятие в этот файл - напишите Александру {{<profile  telegram="grinvind">}}. 


