Build Hugo image
```
docker build . -f Docker-builder -t hugo-builder
```

Run Hugo builder image to test changes locally.
```
docker run --rm -it -v ${pwd}:/src -p 1313:1313 hugo-builder --source /src/ server --bind 0.0.0.0
```